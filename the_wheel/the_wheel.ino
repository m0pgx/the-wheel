// KX3 Serial comms
#include <SoftwareSerial.h>
#include <elapsedMillis.h>

#define BAUD_RATE 38400       // KX3 serial speed
//#define BAUD_RATE 9600       // KX3 serial speed

// Optical encoder
#define  A_PHASE 2
#define  B_PHASE 3


String str = "";
String band;                            // Which band the radio was on the last time it was polled.
unsigned long flag_A = 0;               //Assign a value to the token bit
unsigned long flag_B = 0;               //Assign a value to the token bit
unsigned long fr;                       // Frequency
unsigned long freq;                     // Holds the frequency that the radio will be set to (the frequency of the VFO knob)
int buttons = 5;                        // Analog pin for the six buttons

// There is a delay on the serial port when switching from one to the other so we 
// need to watch for this and add a delay when tuning through these limits
unsigned long dual_limit = 15000;       // Limit (offset) difference between the two VFO's so we know when the dual watch will switch over
bool dual_watch_status;                 // Is the dual watch on or off?

unsigned long temp_fr;                  // Used to tell if the frequency has changed so we know if to send a frequency update to the radio
int freqStep = 1;                       // Size of the step to tune by
String vfo = "A";                       // Which VFO to tune - defaults to B as the purpose of the project is to provide a decent VFO B knob
int dull_wheel = 1;
bool fast = false;
int wheel_step = 10;                    // How many Hz to tune per step
String current_mode;
bool contest_mode = false;              // Are we in contest mode? Enables Dip and Dive when tuning VFO B, this may also limit VFO B to within 23 Khz of VFO A

int tunePeriod = 500;                   // amount of delay between syncing frequency (checks for band change)
elapsedMillis tuneTimeElapsed;

SoftwareSerial computerSerial(10, 11, true); // The computer

void setup()
{
  Serial.begin(BAUD_RATE);              // Default Aarduino port
  Serial2.begin(BAUD_RATE);             // The radio
  computerSerial.begin(BAUD_RATE);             // The computer
  // Optical encoder
  pinMode(A_PHASE, INPUT);
  pinMode(B_PHASE, INPUT);
  attachInterrupt(digitalPinToInterrupt(A_PHASE), tune, RISING); //Interrupt for the optical encoder
  
  freq = getFrequency();                // Set the initial frequency to that of the radio
  band = getValue("BN;");               // Get the current band number
  dual_watch_status = getDualWatchStatus(); // Get the status of the dual watch

  pinMode(6, INPUT);
  pinMode(7, INPUT);
  pinMode(8, INPUT);
  pinMode(9, INPUT);
  initVfo();
  sendText("    Ready ", 0);
//  playSound("k", 1500);
}

void loop()
{
  addTuneOffsetToFrequency();         // Takes the amount the VFO was turned and adds it to the current frequency
  setFrequency(freq);                 // Set the new frequency (if the frequency has changed)
  syncFrequency();                    // Periodically set the wheel frequency to that of the radio
  checkButtons();
  passTwoThree();
}

void passTwoThree()
{
  int pass;

  // Pass all from computer to radio
  while (computerSerial.available()) {     // If anything comes in Serial1 (pins 0 & 1)
    pass = computerSerial.read();
    Serial2.write(pass);   // read it and send it out Serial (USB)
    Serial.write(pass);

    if(pass == ';') {
      Serial.println("");
    }
  }

  // Pass all from radio to computer
  while (Serial2.available()) {      // If anything comes in Serial (USB),
    pass = Serial2.read();
    computerSerial.write(pass);   // read it and send it out Serial1 (pins 0 & 1)
    Serial.write(pass);

    if(pass == ';') {
      Serial.println("");
    }
  }
}

void sendText(String message, int del)
{
  if(message != "")
  {
    for(int pos = 0; pos < message.length(); pos++)
    {
      Serial2.println("DB" + String(message.charAt(pos)) + ";");
      delay(del);
    }
  }
}

void pushText(String message)
{
  if(message != "")
  {
    for(int pos = 0; pos < message.length(); pos++)
    {
      Serial2.println("DB" + String(message.charAt(pos)) + ";");
    }
  }
}

/*
 * Plays a sound in CW
 * text  - the text to play
 * del  - how long to wait for the sound to have played (this MUST be long enough!)
 */
void playSound(String text, int del)
{
  current_mode = getValue("MD;");     // Get the current mode
  String pwr = getValue("PC;");       // Get the current power (this is rounded by the radio)
  Serial2.println("PC000;MD3;KYW<" + text + ";" + pwr + ";");   // Chenge power to 0, change mode, play the text, set the power back to what it was
  delay(del);
  Serial2.println(current_mode + ";");  // Change back to the previous mode
  delay(100);
}

void addTuneOffsetToFrequency()
{
  freq += flag_A;                     // Get the amount the encoder has changed and add it to the frequency
  flag_A = 0;                         // Clear the amount the enconder has changed
}

void checkButtons()
{
  int buttonTrigger = getButton();
//Serial.println(buttonTrigger);

  if ((buttonTrigger > 10 && buttonTrigger < 950) || (buttonTrigger > 1500 && buttonTrigger < 2400))
  {
    if (buttonTrigger > 500 && buttonTrigger < 520) {               // ONE
        cycleVfo();
    } else if (buttonTrigger > 670 && buttonTrigger < 723) {        // TWO
        cycleStepSize();
    } else if (buttonTrigger > 724 && buttonTrigger < 807) {        // THREE
      delay(100);
      Serial2.println("SWT24;");                                    // A/B
      Serial2.println("SWT25;");                                    // A=>B
      sendText("  B to A", 0);
    } else if (buttonTrigger > 807 && buttonTrigger < 830) {        // FOUR
        sendText("  Btn 4", 0);
    } else if (buttonTrigger > 830 && buttonTrigger < 865) {        // FIVE
        sendText("  Btn 5", 0);
    } else if (buttonTrigger > 865 && buttonTrigger < 888) {        // SIX
        sendText("  Btn 6", 0);
    }
  
    // Long presses
    else if (buttonTrigger > 2000 && buttonTrigger < 2020) {        // LP ONE
        sendText("   LP 1", 0);
    } else if (buttonTrigger > 2170 && buttonTrigger < 2223) {      // LP TWO
        wheel_step = 5000;
        sendText("5khz   ", 0);
    } else if (buttonTrigger > 2224 && buttonTrigger < 2307) {      // LP THREE
        sendText("   LP 3", 0);
    } else if (buttonTrigger > 2307 && buttonTrigger < 2330) {      // LP FOUR
        sendText("   LP 4", 0);
    } else if (buttonTrigger > 2314 && buttonTrigger < 2365) {      // LP FIVE
        sendText("   LP 5", 0);
    } else if (buttonTrigger > 2365 && buttonTrigger < 2388) {      // LP SIX
        sendText("   LP 6", 0);
    }
  }
}

int getButton()
{
  int buttonValue = 0;
  uint32_t ts1;
  uint32_t ts2;

  ts1 = millis();
  while(analogRead(buttons) < 900)
  {
    buttonValue = analogRead(buttons);
    delay(5);
    ts2 = millis();
  }

  if (ts2 - ts1 > 900)      // Looking for a long press of about 1 second (900 miliseconds)
  {
    buttonValue += 1500;
  }

  if (buttonValue < 900 || buttonValue > 1500)
  {
    return buttonValue;
  }
  else
    return 0;
}

void cycleVfo()
{
  if(vfo == "A")
    vfo = "B";
  else
    vfo = "A";

  sendText("   VFO " + String(vfo), 0);
  initVfo();
}

void cycleStepSize()
{
  if(wheel_step == 10)
  {
    wheel_step = 100;
    sendText("100hz  ", 0);
  }
  else if(wheel_step == 100)
  {
    wheel_step = 1;
    sendText("1hz    ", 0);
  }
  else
  {
    wheel_step = 10;
    sendText("10hz   ", 0);
  }
}

void initVfo()
{
  freq = getFrequency();
  delay(100);
}

void setFrequency(unsigned long new_freq)
{
  String new_str = "";
  String pad;
  
  if(temp_fr != new_freq)
  {
    if(new_freq >= 100000000)
      pad = "00";
    else if(new_freq >= 10000000)
      pad = "000";
    else
      pad = "0000";
      
    new_str = "F" + vfo + pad + String(new_freq) +";";

    if(wheel_step == 100)
    {
      new_str = new_str.substring(0,11) + "00;";    // Zero the last two digits of the frequency
    }
    if(wheel_step == 10)
    {
      new_str = new_str.substring(0,12) + "0;";     // Zero the last digit of the frequency
    }

    Serial2.println(new_str);
    temp_fr = freq;
    delay(50);                                     // Delay so the radio can cope with the timing of the signals
  }
}

void setVfoAFrequency(unsigned long new_freq)
{
  String new_str = "";
  String pad;
  
  if(temp_fr != new_freq)
  {
    if(new_freq >= 100000000)
      pad = "00";
    else if(new_freq >= 10000000)
      pad = "000";
    else
      pad = "0000";
      
    new_str = "FA" + pad + String(new_freq) +";";

    if(wheel_step == 100)
    {
      new_str = new_str.substring(0,11) + "00;";    // Zero the last two digits of the frequency
    }
    if(wheel_step == 10)
    {
      new_str = new_str.substring(0,12) + "0;";     // Zero the last digit of the frequency
    }

    Serial2.println(new_str);
    temp_fr = freq;
    delay(50);                                     // Delay so the radio can cope with the timing of the signals
  }
}

unsigned long getFrequency()
{
  long the_freq;
  char ch;
  String the_frequency = "";
  String swap = "F" + vfo + ";";    // Set to read which ever VFO is set

  the_frequency = getValue(swap);
  the_freq = the_frequency.substring(2,13).toInt();
  Serial.println("getFrequency: " + the_frequency + " : " + the_freq);
  
  return the_freq;
}

bool getDualWatchStatus()
{
  if(getValue("SB;") == "SB1")
    return true;

  return false;
}

/*
 * Gets the value for a 'code' from the radio
 */
String getValue(String v)
{
  char ch;
  String the_value = "";

  Serial2.flush();
  Serial2.println(v);
  delay(120);                        // Needs a delay so the radio has time to respond
  
  while(Serial2.available() > 0)
  {
    ch = Serial2.read();

    if (ch != ';') 
    {
      the_value += ch;
    }
  }

  return the_value;
}

/*
 * Detects rotation of the optical encoder
 * and also de-sensitises the encoder to
 * one third of its 400 point per revolution
 * sensitivity
 */
void tune()                       // Get the amount and direction that the optical encoder has rotated
{
  if(dull_wheel == 1 || fast)     // If we are in fast mode, or at a read point of the dulling down process
  {
    char i;                       // Holds the second status of the optical encoder
    i = digitalRead(B_PHASE);      // Read the second status of the optical encoder
    if (i == 1)
      flag_A -= wheel_step;        // If it's 1 then tune down
    else
      flag_A += wheel_step;        // If it's 1 then tune up
  }

/* 
  Change the number in the if to change the sensitivity of the VFO 
*/
  if(dull_wheel == 3)             // If encoder has moved by 3 points
    dull_wheel = 0;               // Reset the counter

  dull_wheel ++;                  // Increment the dulling down counter
  tuneTimeElapsed = 0;
}

/*  
 *   Periodically sets the knob 
 *   frequency to that of the 
 *   radio
 */
void syncFrequency()
{
  long freqValue;
  
  if(tuneTimeElapsed > tunePeriod)
  {
    freqValue = getFrequency();
    if(freqValue == 0)
    {
      setFrequency(freq);
    } else {
      freq = freqValue;
      fr = freq;
    }
    tuneTimeElapsed = 0;
  }
}

String formatFrequency(String vfo) {
  String freq = "";

  // e.g. convert '07' to '7'
  freq += String(vfo.substring(5, 7).toInt());

  //freq += ".";
  freq += vfo.substring(7, 10);
  freq += ".";
  freq += vfo.substring(10, 12);
  Serial.println(freq);

  return freq;
}
