# Update
Serial passthrough is now working (which you will need if you expect to use a computer with your KX3) using a hardware port to talk to the radio and SoftwareSerial to talk to aux (in my case the computer). Using a level converter for the hardware port in order to invert the signal. Defined the software serial port as inverted.

# Arduino
For this I used an Arduino Mega 2560.

# Optical Encoder
I used a cheap one from China (about �8). 
The encoder has 4 wires and a braid. Ignore the braid.
Red = vcc
Black = ground
White = output A
Green = output B

Red to 5v
Black to ground
White to pin 2
Green to pin 3
Pull-up resistor from vcc (5v) to pin 2
Pull-up resistor from vcc (5v) to pin 3
